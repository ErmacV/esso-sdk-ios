//
//  MainTabbarConroller.swift
//  TestApp
//
//  Created by 1 on 10/8/19.
//  Copyright © 2019 TBWAMobile. All rights reserved.
//

import UIKit
import EssoSDK

class MainTabbarController: UITabBarController {
    
    var rewardsListController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewControllers()
    }
    
    func updateViewControllers() {
        //rewards
        rewardsListController = EssoSDK.shared.rewardsController(self)
        rewardsListController?.tabBarItem = UITabBarItem(title: "Rewards", image: nil, tag: 11)
        //Second
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondController = storyboard.instantiateViewController(withIdentifier: "SecondController")
        secondController.tabBarItem = UITabBarItem(title: "Web&Banner", image: nil, selectedImage: nil)
        //Help
        let helpController = storyboard.instantiateViewController(withIdentifier: "HelpContrroller")
        helpController.tabBarItem = UITabBarItem(title: "Login", image: nil, selectedImage: nil)
        self.viewControllers = [rewardsListController!, secondController, helpController]
    }
}

extension MainTabbarController: RewardsListDelegate {
    
    func rewardSelected(rewardURL: String) {
        //open webview
        let controller = EssoSDK.shared.rewardDetailsController(rewardURL: rewardURL)
        rewardsListController?.present(controller, animated: true, completion: nil)
    }
    
    func getLanguage() -> String {
        return "nl"
    }
    
    func getRegion() -> String {
        return "nl"
    }
}
