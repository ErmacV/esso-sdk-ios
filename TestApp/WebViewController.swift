//
//  WebViewController.swift
//  TestApp
//
//  Created by 1 on 9/25/19.
//  Copyright © 2019 Artem. All rights reserved.
//

import UIKit
import EssoSDK

enum WebviewId: Int {
    case forgottenEmail
    case forgottenPassword
    case transactions
    case manageCards
    case rewardDetailsScreen
    case changePhoneNumber
    case basket
}

class WebViewController: UITableViewController {
    
    var webNavController: UINavigationController?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case WebviewId.forgottenEmail.rawValue:
            openWebview(.forgottenEmail)
        case WebviewId.forgottenPassword.rawValue:
            openWebview(.forgottenPassword)
        case WebviewId.transactions.rawValue:
            openWebview(.transactions)
        case WebviewId.manageCards.rawValue:
            openWebview(.manageCards)
        case WebviewId.rewardDetailsScreen.rawValue:
            openWebview(.rewardDetails)
        case WebviewId.changePhoneNumber.rawValue:
            openWebview(.changePhoneNumber)
        case WebviewId.basket.rawValue:
            openWebview(.basket)
        default:
            print("not the case")
        }
    }
    
    func openWebview(_ screenId: WebPageId) {
        self.webNavController = EssoSDK.shared.getWebControllerWithNavigation(webDelegate: self, pageId: screenId)
        self.present(self.webNavController!, animated: true, completion: nil)
    }
}

extension WebViewController: WebContorollerDelegate {
    
    func getLanguage() -> String {
        return "nl"
    }
    
    func getRegion() -> String {
        return "NL"
    }
    
    func getCardNumber() -> String {
        return "1223123123411"
    }
    
    func getRewardLink() -> String {
        return "https://www.test-esso-extra.nl//bol-com-cadeaukaarten.html"
    }
    
    func getEmail() -> String {
        return "natalia.cherkashenko@tbwamobile.com"
    }
    
    func phoneUpdated() {
        
    }
    
    func continueShopping() {
        
    }
    
    func userWantsToCloseController() {
        self.webNavController?.dismiss(animated: true, completion: nil)
    }
}
