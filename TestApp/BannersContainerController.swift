//
//  WebContainerController.swift
//  TestApp
//
//  Created by 1 on 9/25/19.
//  Copyright © 2019 Artem. All rights reserved.
//

import UIKit
import EssoSDK

class BannersContainerController: UIViewController {
    
    @IBOutlet var bannersContainer: UIView!
    
    lazy var bannersView: UIView = {
        let promView = EssoSDK.shared.promotionalBannersView(.zero,
                                                             bannersDelegate: self)

        return promView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bannersContainer.addSubview(bannersView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bannersView.backgroundColor = .red
        bannersView.frame = CGRect(x: 0, y: 0,
                                   width: bannersContainer.bounds.size.width,
                                   height: bannersContainer.bounds.size.height)
        bannersView.layoutIfNeeded()
    }
}

extension BannersContainerController: PromotionalBannersDelegate {
    func bannerSelected(url: String) {
        let webController = EssoSDK.shared.getWebcontroller(url)
        self.present(webController, animated: true, completion: nil)
    }
    
    func bannersLocale() -> String {
        return "nl_NL"
    }
}
