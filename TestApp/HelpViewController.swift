//
//  HelpViewController.swift
//  TestApp
//
//  Created by 1 on 10/8/19.
//  Copyright © 2019 TBWAMobile. All rights reserved.
//

import UIKit
import Alamofire

struct JSONBodyParams: ParameterEncoding {
    let jsonData: Data?
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = jsonData
        return request
    }
}

class HelpViewController: UIViewController {

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginBe() {
        login(email: "cmartinezmurcia3@gmail.com", password: "123goNOW2019+", country: "BE")
    }
    
    @IBAction func lgoin() {
        login(email: "natalia.cherkashenko@tbwamobile.com", password: "Mobile1234!", country: "NL")
    }
    
    private func login(email: String, password: String, country: String) {
        activityIndicator.startAnimating()
        let loginRequest = "https://esso-backendapi-dev-acceptance.azurewebsites.net/api/Auth/login"
        guard let loginUrl = URL(string: loginRequest) else { return }
        let params = ["email": email,
                      "cardNumber": "",
                      "password": password,
                      "country": country]
        let jsonData = try? JSONSerialization.data(withJSONObject: params)
        let headers = ["content-type": "application/json"]
        Alamofire.request(loginUrl,
                          method: .post,
                          parameters: [:],
                          encoding: JSONBodyParams.init(jsonData: jsonData),
                          headers: headers).responseJSON { [weak self] response in
            self?.activityIndicator.stopAnimating()
            guard response.result.isSuccess,
                let responseDict = response.result.value as? [String: Any] else {
                self?.showError()
                return
            }
            let payload = responseDict["payload"] as? [String: String]
            let accessToken = payload?["accessToken"]
            UserDefaults.standard.set(accessToken, forKey: "AccessToken")
            self?.showSuccess()
        }
    }
    
    private func showError() {
        let alert = alertController(title: "Not Logged In", message: "Login failed")
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showSuccess() {
        let alert = alertController(title: "Logged In", message: "Login was successful")
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertController(title: String, message: String) -> UIAlertController {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        controller.addAction(okAction)
        return controller
    }
}
